require.config({
baseUrl:'js',
shim: {
    underscore: {
      exports: '_'
    },
    backbone: {
      deps: ["underscore", "jquery"],
      exports: "Backbone"
    }
  },
paths:{
jquery:'lib/jquery',
underscore: 'lib/underscore',
backbone: 'lib/backbone',
jquerymobile: 'lib/jquery.mobile',
templates:'../templates'
}

});

require([
  // Load our app module and pass it to our definition function
  'main',
], function(Main){
 
  
 Main.initialize();
 
 
 
 
});


