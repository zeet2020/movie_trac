define([
  'jquery',
  'underscore',
  'backbone',
  'jquerymobile',
  'text!templates/homeview.html'
], function($, _, Backbone, jqmunused, homeview_tpl){
  
  var Views = {};
  
  
  Views.HomeListView = Backbone.View.extend({
    el: '#content',
    render: function(){
      // Using Underscore we can compile our template with data
      var data = {};
      var compiledTemplate = _.template( homeview_tpl, data );
      // Append our compiled template to this Views "el"
      this.$el.append( compiledTemplate );
	  
    }
  });
  // Our module now returns our view
  return Views;
});