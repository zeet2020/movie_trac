// Filename: router.js

define([
  'jquery',
  'underscore',
  'backbone',
  'jquerymobile',
  'views',  
], function($, _, Backbone,jqmunused, Views){
  

  var initialize = function(){
     var homepage = new Views.HomeListView();
	 homepage.render();
	 homepage.$el.find('ul').listview();
    //Backbone.history.start();
  };
  return {
    initialize: initialize
  };
});