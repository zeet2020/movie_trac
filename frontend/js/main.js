

define([
  'jquery',
  'underscore',
  'backbone',
  'jquerymobile',
  'router', // Request router.js
], function($, _, Backbone, jqmunused, Router){
  var initialize = function(){
    // Pass in our Router module and call it's initialize function
    Router.initialize();
  }

  return {
    initialize: initialize
  };
});





