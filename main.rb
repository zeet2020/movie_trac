require 'sinatra'
require 'rubygems'
require 'data_mapper'
require 'json'
#require 'dm_mysql_adapter'


DataMapper.setup(:default, 'mysql://root@localhost/movietrac')


###models

class Movie 
 include DataMapper::Resource
 
 property :id,    Serial
 property :title, String
 property :lang, String
 property :image_url, String
 property :director, String
 property :actors, String
 property :music, String
 property :release_date, DateTime
 property :description, Text
 property :created_at, DateTime
 property :updated_at, DateTime
 
 validates_presence_of :title
 validates_length_of :body, :minimum => 1
 
 has n, :comments
 
 end
 
 class Comment 
 include DataMapper::Resource
 
 property :id,    Serial
 property :email, String
 property :comment, Text
 property :rating, Integer
 property :movie_id, Integer
 property :created_at, DateTime
 property :updated_at, DateTime
 
 validates_presence_of :email
 
 belongs_to :movie
 
 
 end
 
 
 DataMapper.auto_upgrade!
 
 
 
 #controller start here
 
 
 helpers do 
 
  def json_status(code,reason)
    status code
	{
	  :status => code,
	  :message => reason
	}.to_json
  
  end 
 
  def accept_params(params, *fields)
      h = { }
      fields.each do |name|
        h[name] = params[name] if params[name]
      end
      h
   end
 
 end 
 
 get '/' do
 
    "welcome to the movie trac "
 
 end 
 
 
 #code the list the movies 
 get '/movies' do
 content_type :json
 
   if movies = Movie.all
      movies.to_json
	else
	  json_status 404, "not found"
	end 
 
 end 
 
 #movies create post method
 
 post '/movies' do 
    content_type :json
    movie_params = accept_params(params,:title, :lang, :image_url, :director, :actors, :music, :description)
	movie = Movie.new(movie_params)
	if movie.save
	 status 201
	  else
	  json_status 400, movie.errors.to_hash
	end 
 end 
 
 
 
 #movies details 
 get '/movies/:id' do
 content_type :json
 
  #if Movie.valid_id?(params['id'])
     if movie = Movie.first(:id => params[:id].to_i)
        movie.to_json
      else
        json_status 404, "Not found"
      end
   #else
      # TODO: find better error for this (id not an integer)
      #json_status 404, "Not found"
    #end
 end
 
 
 #comments for the movies by users
 get '/movies/:id/comments' do
    content_type :json
   halt json_status 404, "Not found" unless Movie.first(:id => params[:mid].to_i)
   movie = Movie.first(:id => params[:mid].to_i)
   if comments = movie.comments.all
      comments.to_json
	else
	  json_status 404, "not found"
	end 
 end 
 
 #comment create post
 post '/movies/:mid/comments' do 
     content_type :json
    
	 halt json_status 404, "Not found" unless Movie.first(:id => params[:mid].to_i)
 
    comment_params = accept_params(params,:email, :comment, :rating)
	comment_params[:movie_id] = params[:mid]
	comment = Comment.new(comment_params)
	if comment.save
	 status 201
	else
	  json_status 400, comment.errors.to_hash
	end 
end
 
 
 